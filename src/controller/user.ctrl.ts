import UserRepository from "./../repository/user.repository";
import { IUser } from "../model/user";
import BaseController = require("./base.ctrl");

export class UserController extends BaseController<IUser> {

    constructor() {
        super(new UserRepository());
    }
    
}
