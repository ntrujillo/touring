import { Request, Response } from "express";

import mongoose = require("mongoose");
import { getLogger } from 'log4js';
import BaseRepository = require("../repository/base.repository");
import { Pagination } from "../model/pagination";

const logger = getLogger("BaseController");

abstract class BaseController<T extends mongoose.Document> {

    public _repository: BaseRepository<T>;

    constructor(repository: BaseRepository<T>) {
        this._repository = repository;
    }


    create = async (req: Request, res: Response) => {

        try {          
            let objectParam: T = <T>req.body;
            logger.debug("Start create", objectParam);
            let objectCreated = await this._repository.create(objectParam);
            res.send(objectCreated);
        }
        catch (e) {
            logger.error(e);
            res.status(500).send(e.message);

        }
    }

    update = async (req: Request, res: Response) => {

        try {
            var objectParam: T = <T>req.body;
            logger.debug("Start update id: ", req.params._id);
            let objectUpdated = await this._repository.update(this.toObjectId(req.params._id), objectParam);
            res.send(objectUpdated);
        }
        catch (e) {
            logger.error(e);
            res.status(500).send(e.message);

        }
    }
    
    delete = async (req: Request, res: Response) => {

        try {
            logger.debug("Start delete id: ", req.params._id);
            await this._repository.delete(req.params._id);
            res.send({})
        }
        catch (e) {
            logger.error(e);
            res.status(500).send(e.message);

        }
    }

    retrieve = async (req: Request, res: Response) => {

        try {
            let pageRequest = new Pagination(req);
            logger.debug("Start retrive", pageRequest);
            let response: any = await this._repository.retrieve(pageRequest);
            res.header('X-Total-Count', response.total);
            res.send(response.data);
        }
        catch (e) {
            logger.error(e);
            res.status(500).send(e.message);

        }
    }

    findById = async (req: Request, res: Response) => {
        
        try {
            logger.debug("Start findById: ",req.params._id);
            let objectFound = await this._repository.findById(req.params._id);
            res.send(objectFound);
        }
        catch (e) {
            logger.error(e);
            res.status(500).send(e.message);

        }
    }

    private toObjectId(_id: string): mongoose.Types.ObjectId {
        return mongoose.Types.ObjectId.createFromHexString(_id)
    }


}
export = BaseController;    