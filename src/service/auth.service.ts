import { IUser } from "../model/user";
import UserRepository = require("../repository/user.repository");
import jwt = require('jsonwebtoken');
import bcrypt from "bcryptjs";
import ServiceException = require("./service.exception");


class AuthService {


    private _userRepository: UserRepository;


    constructor() {
        this._userRepository = new UserRepository();
    }

    register = async (user: IUser, password: string) => {
        user.hash = bcrypt.hashSync(password, 10);
        return this._userRepository.create(user);
    }


    authenticate = async (email: string, password: string) => {

        let user: IUser = await this._userRepository.findOne({ email: email });

        if (!user)
            throw new ServiceException(404, "Usuario no econtrado");

        if (!user.active)
            throw new ServiceException(403, "Usuario no esta activo");

        if (!bcrypt.compareSync(password, String(user.hash)))
            throw new ServiceException(403, "Password invalido");


        let object = user.toObject();

        object.token = this.createToken(user);;

        return object;
    }

    private createToken(user: IUser) {

        const expiresIn = 60 * 60 * 24; // 24 hours
        const secret = process.env.SECRET || '';
        const dataStoredInToken = {
            sub: user._id
        }

        return jwt.sign(dataStoredInToken, secret, { expiresIn });
    }
}


Object.seal(AuthService);
export = AuthService;
