import app from "./app";

app.listen(process.env.PORT,
     () => console.log(`API Rest Listening on port http://localhost:${process.env.PORT}`));