import { Document, Schema, Model, model } from "mongoose";

export interface IUser extends Document {   
    firstName: string;
    lastName: string;
    email: string;
    phone: Number;
    createAt: Date;
    active: Boolean;
    hash: string;
    pictureUrl: string;
    country: string;
    city: string;
    postal: string;
    address: string;
    token:string;
}

let UserSchema = new Schema({    
    firstName: {
        type: String,
        required: false,
        trim: true
    },
    lastName: {
        type: String,
        required: false,
        trim: true
    },
    email: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    phone: {
        type: String,
        required: false,
        trim: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now
    },
    active: {
        type: Boolean,
        required: true,
        default: true
    },
    hash: {
        type: String,
        required: false
    },
    pictureUrl: {
        type: String,
        required: false
    },
    country: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: false
    },
    postal: {
        type: String,
        required: false
    },
    address: {
        type: String,
        required: false
    }

});


UserSchema.index({email: 1 }, { unique: true });

export const User: Model<IUser> = model<IUser>("User", UserSchema);
