
export class Pagination {

    page: number;
    pageSize: number;
    fields: string;
    sort: string;

    constructor(request: any) {       
        this.page = request.query.page;
        this.pageSize = request.query.per_page;
        this.fields = request.query.fields;
        this.sort = request.query.sort;
    }

}