import express, { Application } from 'express';
import bodyParser from 'body-parser';
import { configure } from 'log4js';
import { config } from "dotenv";
import path from 'path';
import cors from 'cors';
import mongoose from 'mongoose';
import { UserRoutes } from './routes/user.route';
import { AuthRoutes } from './routes/auth.route';


class App {

  public app: Application;

  constructor() {
    //application with express
    this.app = express();
    //config static public folder for Angular App files
    this.app.use(express.static(path.join(__dirname, 'public')));

    //setting general config
    this.setConfig();

    //mongodb connection
    this.setMongoConfig();

    //API Rest routes definition
    this.routes();
  }

  private routes(): void {
    this.app.use("/api/user", new UserRoutes().router);
    this.app.use("/auth", new AuthRoutes().router);
  }

  private setConfig() {
    //initializations
    configure(__dirname + '/config/log4js.json');
    config({ path: '.env' });
    this.app.use(bodyParser.json({ limit: '50mb' }));
    this.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    this.app.use(cors());
    //Seteo en middleware cabecera de respuesta
    this.app.use((req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, UPDATE, DELETE');
      res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
      res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
      next();
    });    
  }

  //Connecting to our MongoDB database
  private setMongoConfig() {
    mongoose.Promise = global.Promise;
    mongoose.set('useCreateIndex', true);
    mongoose.connect(process.env.DATABASE || '', {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
  }
}

export default new App().app;