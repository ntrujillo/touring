import mongoose = require("mongoose");
import { Pagination } from "../model/pagination";

class BaseRepository<T extends mongoose.Document>  {

    private _model: mongoose.Model<mongoose.Document>;

    constructor(schemaModel: mongoose.Model<mongoose.Document>) {
        this._model = schemaModel;
    }

    create(item: T): Promise<T> {
        return new Promise((resolve, reject) => {
            this._model.create(item, (error: any, result: T) => {
                if (error) reject(error)
                else resolve(result)
            });
        })
    }

    retrieveAll() {
        return new Promise((resolve, reject) => {

            this._model.find({}, (error, result) => {
                if (error) reject(error)
                else resolve(result)
            });

        });

    }

    retrieve(pagination: Pagination) {

        let plus = /\+/g;
        let comma = /\,/g;
        let criteria = {};

        if (pagination.sort) {
            pagination.sort = pagination.sort.replace(plus, '');
            pagination.sort = pagination.sort.replace(comma, ' ');
        }
        if (pagination.fields) {
            pagination.fields = pagination.fields.replace(comma, ' ');
        }

        return new Promise((resolve, reject) => {

            let response: any = {};

            this._model.find(criteria).countDocuments((error, count) => {

                if (error)
                    reject(error)
                else {
                    this._model.find(criteria)
                        .skip(pagination.pageSize * pagination.page)
                        .limit(pagination.pageSize)
                        .exec((error, result) => {
                            if (error) reject(error)
                            else {
                                response.total = count;
                                response.data = result;
                                resolve(response)
                            }
                        })
                }

            });
        });
    }


    update = (_id: mongoose.Types.ObjectId, item: T) => {
        return this._model.updateOne({ _id: _id }, item);
    }

    delete(_id: string) {
        return this._model.deleteOne({ _id: this.toObjectId(_id) });
    }

    findById(_id: string): Promise<T> {
        return new Promise((resolve, reject) => {
            this._model.findById(this.toObjectId(_id), (error, result: T) => {
                if (error) reject(error)
                else resolve(result);
            });
        });
    }

    findOne(searchCriteria: any): Promise<T> {
        return new Promise((resolve, reject) => {
            this._model.findOne(searchCriteria, (error: any, result: T) => {
                if (error) reject(error)
                else resolve(result);
            });
        });
    }

    public toObjectId(_id: string): mongoose.Types.ObjectId {
        return mongoose.Types.ObjectId.createFromHexString(_id)
    }

}

export = BaseRepository;