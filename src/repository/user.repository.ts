import { IUser, User } from "../model/user";
import BaseRepository from "./base.repository";

class UserRepository extends BaseRepository<IUser> {
    constructor() {
        super(User);
    }   
}

Object.seal(UserRepository);
export = UserRepository;