#!/bin/sh
echo "Building app prod version can take some minutes ..."
npm run build
echo "Compress ..."
tar -czvf ~/api.tar.gz dist/
echo "Sending data ..."
sftp groupmobil << EOF
lcd ~
put api.tar.gz
bye
EOF
echo "Deploying ..."
ssh groupmobil << EOF
tar -xzvf api.tar.gz
cp -R dist/* ~/APP_HOME/
cp dist/.env ~/APP_HOME/
rm api.tar.gz
rm -R dist
exit
EOF
echo "Cleaning build"
rm ~/api.tar.gz
echo "Finish script."