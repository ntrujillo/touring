### Api Rest with NodeJS && Express Example

## API User

http://locahost:3000/api/user GET retrieve all users

http://locahost:3000/api/user POST create a user

```json
{

	"firstName":"Nelson",
	"lastName":"Trujillo",
	"email":"ntrujillo@mailinator.com",
	"phone":"099999999"

}
```



http://locahost:3000/api/user/{_id} PUT update a user with _id

http://locahost:3000/api/user/{_id} DELETE delete a user with _id


## Start server

Requiere .env file with the process environment variables

```
DATABASE = urltomongodb
PORT = 3000
```

Start Server
```bash
npm start
```

![Image of .env](docs/env.png "Logo Title Text 1")